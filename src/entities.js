class Ball {
  constructor(x, y, radius, image) {
    this.radius = radius;
    this.x = x;
    this.y = y;
    this.dx = 1;
    this.dy = 1;
    this.img = image;
    // this.resize_image();
    this.bb = this.get_bb();
    this.type = "ball";
  }
  resize_image(){
      this.img.width = `${this.radius}px`;
      this.img.height = `${this.radius}px`;
  }
  draw() {
    ctx.drawImage(this.img, this.x - 5, this.y -5);

  }

  get_bb() {
    return [this.y, this.x + this.radius, this.y + this.radius, this.x];
  }

  update(colliders, deltaTime) {
    for (let i = 0; i < colliders.length; i++) {
      if (colliders[i] != undefined && this.check_collision(colliders[i])) {
        if (colliders[i].type === "brick") colliders[i].health -= 1;
        break;
      }
    }

    if (this.collisionX()) {
      this.dx *= -1;
    }
    if (this.collisionY()) {
      this.dy *= -1;
    }

    this.x = this.x  + this.dx;
    this.y = this.y + this.dy;
    // console.log(this.x, this.y, deltaTime);
    this.draw();
  }

  collisionX() {
    return this.x + this.radius >= canv.width || this.x - this.radius <= 0;
  }
  collisionY() {
    return this.y + this.radius >= canv.height || this.y - +this.radius <= 0;
  }

  check_collision(obj) {
    let coll = false;

    // check if bounce is horizontal
    if (
      this.x + this.radius + this.dx > obj.x &&
      this.x + this.dx < obj.x + obj.w &&
      this.y + this.radius > obj.y &&
      this.y < obj.y + obj.h
    ) {
      this.dx *= -1;
      coll = true;
    }

    // check if bounce is vertical
    if (
      this.x + this.radius > obj.x &&
      this.x < obj.x + obj.w &&
      this.y + this.radius + this.dy > obj.y &&
      this.y + this.dy < obj.y + obj.h
    ) {
      this.dy *= -1;
      coll = true;
    }

    return coll;
  }
}

class Paddle {
  constructor(x, y, length, height, image) {
    this.w = length;
    this.h = height;
    this.x = x;
    this.y = y;
    this.img = image;
    this.bb = this.get_bb();
    this.dx = 0;
    this.type = "paddle";
  }

  draw() {
    ctx.drawImage(this.img, this.x - 1, Math.floor(this.y - 7));
  }

  get_bb() {
    return [this.y, this.x + this.radius, this.y + this.radius, this.x];
  }

  update(keypress, deltaTime) {
    if (keypress[1] == 1 && !this.collisionR()) {
      this.dx = 5;
    } else if (keypress[3] == 1 && !this.collisionL()) {
      this.dx = -5;
    } else {
      this.dx = 0;
    }

    this.x = this.x  + (this.dx);
    this.draw();
  }

  collisionL() {
    return this.x <= 0;
  }

  collisionR() {
    return this.x + this.w >= canv.width;
  }
}

class Brick {
  constructor(x, y, length, height, health, color, image) {
    this.w = length;
    this.h = height;
    this.x = x;
    this.y = y;
    this.img = image;
    this.bb = this.get_bb();
    this.dx = 0;
    this.color = color;
    this.health = health;
    this.type = "brick";
  }

  draw() {
    ctx.drawImage(this.img, this.x, this.y);

  }

  get_bb() {
    return [this.y, this.x + this.radius, this.y + this.radius, this.x];
  }
}
