const canv = document.getElementById("scene");
const ctx = canv.getContext("2d");

let ball = null;
let paddle = null;
let bricks = [];
let arrow_keys = [0, 0, 0, 0];
let colliders = [];
let prev_ticks = 0;
let ticks = 0;
let current_level = 0;

const mouse = {
  x: null,
  y: null,
};

window.addEventListener("resize", function () {
  // canv.width = window.innerWidth;
  // canv.height = window.innerHeight;
  init();
});

window.addEventListener("keydown", this.handle_keypress, false);

function handle_keypress(e) {
  switch (e.keyCode) {
    case 37:
      arrow_keys[3] = 1;
      break;
    case 39:
      arrow_keys[1] = 1;
      break;
    case 40:
      arrow_keys[2] = 1;
      break;
    case 38:
      arrow_keys[0] = 1;
  }
}

function init() {
  ball = new Ball(Math.round(canv.width / 2), canv.height - 25, 1, sprites['ball']);
  paddle = new Paddle(Math.round(canv.width / 2) - 30, canv.height - 10, 60, 2, sprites['paddle']);
  colliders.push(paddle);

  let level = levels[current_level];
  for (let i = 0; i < level.length; i++) {
    let x_coord = 0;
    let y_coord = 0;
    let brick_width = Math.round(canv.width / level[i].length);
    let brick_height = Math.round(canv.height / 2 / level.length);

    for (let j = 0; j < level[i].length; j++) {
      x_coord = j * brick_width;
      y_coord = i * brick_height;
      let brk_id = level[i][j]; 
      if (level[i][j] != 0) {
        let brick = new Brick(
          x_coord,
          y_coord,
          brick_width ,
          brick_height ,
          1,
          "white",
          sprites['bricks'][brk_id - 1]
        );
        bricks.push(brick);
        colliders.push(brick);
      }
    }
  }
}

function animate(ticks) {
  ctx.clearRect(0, 0, canv.width, canv.height);
  let deltaTime = ticks - prev_ticks;
  prev_ticks = ticks;
  
  paddle.update(arrow_keys, deltaTime);
  ball.update(colliders, deltaTime);
  
  
  for (let i = 0; i < bricks.length; i++) {
    if (bricks[i] != undefined)
      if (bricks[i].health <= 0) {
        delete bricks[i];
        delete colliders[i + 1];
      } else {
        bricks[i].draw();
      }
  }

  for (let i = 0; i < arrow_keys.length; i++) {
    arrow_keys[i] = 0;
  }

  requestAnimationFrame(animate);
}

init();
animate();
