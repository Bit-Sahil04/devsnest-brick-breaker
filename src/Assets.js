function load_sprite(url){
    var img = new Image();
    img.src = url;
    return img;
}


var sprites = {
  ball: load_sprite("./sprites/ball.png"),
  bricks: [
    load_sprite("./sprites/brick1.png"),
    load_sprite("./sprites/brick2.png"),
    load_sprite("./sprites/brick3.png"),
    load_sprite("./sprites/brick4.png"),
    load_sprite("./sprites/brick5.png"),
  ],
  paddle: load_sprite("./sprites/paddle.png"),
  heart: load_sprite("./sprites/heart.png"),
  heart_empty: load_sprite("./sprites/heart_empty.png"),
};

var levels = [
  [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
  ],
];
